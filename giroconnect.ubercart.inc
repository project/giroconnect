<?php

/**
 * @file
 * The Ubercart file for the giroconnect module.
 */

function giroconnect_get_pm_settings($pm) {
  $settings = array(
    $pm . '_mid' => variable_get($pm . '_mid', ''),
    $pm . '_pid' => variable_get($pm . '_pid', ''),
    $pm . '_secret' => variable_get($pm . '_secret', ''),
  );
  switch ($pm) {
    case GIROCONNECT_METHOD_GIROPAY:
      break;

    case GIROCONNECT_METHOD_GIROMOBILE:
      break;

    case GIROCONNECT_METHOD_GIROTRUST:
      $lib = new GiroConnectGirotrust();
      $settings[$pm . '_trusteeid'] = variable_get($pm . '_trusteeid', '');
      $settings[$pm . '_feesplit'] = variable_get($pm . '_feesplit', '50');
      break;

    case GIROCONNECT_METHOD_DIRECTDEBIT:
      break;

    case GIROCONNECT_METHOD_CREDITCARD:
      $lib = new GiroConnectCreditcard();
      $settings[$pm . '_cardtypes'] = variable_get($pm . '_cardtypes', array_keys($lib->getCardTypes()));
      $settings[$pm . '_3dsecure'] = variable_get($pm . '_3dsecure', FALSE);
      $settings[$pm . '_paymode'] = variable_get($pm . '_paymode', 'payonly');
      break;

  }
  return $settings;
}

/**
 * Implements hook_uc_payment_method().
 *
 * @return array
 *   TODO
 */
function giroconnect_uc_payment_method() {
  $methods = array();
  $i = 0;
  foreach (GiroConnectTools::paymentMethods() as $pm) {
    $mid = variable_get($pm . '_mid', '');
    $pid = variable_get($pm . '_pid', '');
    $secret = variable_get($pm . '_secret', '');
    $lib = GiroConnect::create($mid, $pid, $secret, GIROCONNECT_SOURCE_ID, $pm);
    $i++;
    $methods[$pm] = array(
      'name' => $lib->getTextName(),
      'title' => $lib->getTextTitle(),
      'desc' => $lib->getTextDescription(),
      'callback' => '_' . $pm . '_uc',
      'weight' => ($i - 11),
      'checkout' => TRUE,
    );
  }
  return $methods;
}

/**
 * Implements hook_order().
 *
 * @param string $op
 *   TODO
 * @param object $order
 *   TODO
 */
function giroconnect_uc_order($op, &$order) {
  if (in_array($order->payment_method, GiroConnectTools::paymentMethods())) {
    switch ($op) {
      case 'submit':
        _giroconnect_uc_save_details($order);
        return _giroconnect_uc_start_payment($order);
        break;

      case 'save':
        _giroconnect_uc_save_details($order);
        break;

      case 'load':
        _giroconnect_uc_load_details($order);
        break;

    }
  }
}

/**
 * TODO
 *
 * @param string $op
 *   TODO
 * @param object $order
 *   TODO
 *
 * @return mixed
 *   TODO
 */
function _gc_giropay_uc($op, &$order = NULL) {
  return _giroconnect_uc(GIROCONNECT_METHOD_GIROPAY, $op, $order);
}

/**
 * TODO
 *
 * @param string $op
 *   TODO
 * @param object $order
 *   TODO
 *
 * @return mixed
 *   TODO
 */
function _gc_giromobile_uc($op, &$order = NULL) {
  return _giroconnect_uc(GIROCONNECT_METHOD_GIROMOBILE, $op, $order);
}

/**
 * TODO
 *
 * @param string $op
 *   TODO
 * @param object $order
 *   TODO
 *
 * @return mixed
 *   TODO
 */
function _gc_girotrust_uc($op, &$order = NULL) {
  return _giroconnect_uc(GIROCONNECT_METHOD_GIROTRUST, $op, $order);
}

/**
 * TODO
 *
 * @param string $op
 *   TODO
 * @param object $order
 *   TODO
 *
 * @return mixed
 *   TODO
 */
function _gc_directdebit_uc($op, &$order = NULL) {
  return _giroconnect_uc(GIROCONNECT_METHOD_DIRECTDEBIT, $op, $order);
}

/**
 * TODO
 *
 * @param string $op
 *   TODO
 * @param object $order
 *   TODO
 *
 * @return mixed
 *   TODO
 */
function _gc_creditcard_uc($op, &$order = NULL) {
  return _giroconnect_uc(GIROCONNECT_METHOD_CREDITCARD, $op, $order);
}

/**
 * TODO
 *
 * @param string $pm
 *   TODO
 * @param string $op
 *   TODO
 * @param object $order
 *   TODO
 *
 * @return mixed
 *   TODO
 */
function _giroconnect_uc($pm, $op, &$order = NULL) {
  switch ($op) {
    case 'cart-details':
      $form = $values = array();
      giroconnect_cart_details($pm, $form, $values);
      return $form;
    case 'cart-process':
      return _giroconnect_uc_cart_process($pm, $order);
    case 'cart-review':
      return _giroconnect_uc_cart_review($pm, $order);
    case 'settings':
      return giroconnect_pm_setting($pm);
  }
  return NULL;
}

/**
 * TODO
 *
 * @param string $pm
 *   TODO
 * @param object $order
 *   TODO
 *
 * @return bool
 *   TODO
 */
function _giroconnect_uc_cart_process($pm, &$order) {
  if (!isset($order->payment_details)) {
    $order->payment_details = array();
  }
  switch ($pm) {
    case GIROCONNECT_METHOD_GIROPAY:
      _giroconnect_uc_cart_process_value($order, 'giroconnect_giropay_bankcode');
      break;

    case GIROCONNECT_METHOD_GIROMOBILE:
      break;

    case GIROCONNECT_METHOD_GIROTRUST:
      break;

    case GIROCONNECT_METHOD_DIRECTDEBIT:
      _giroconnect_uc_cart_process_value($order, 'giroconnect_directdebit_bankcode');
      _giroconnect_uc_cart_process_value($order, 'giroconnect_directdebit_bankaccount');
      break;

    case GIROCONNECT_METHOD_CREDITCARD:
      _giroconnect_uc_cart_process_value($order, 'giroconnect_creditcard_cardtype');
      break;

  }
  return TRUE;
}

/**
 * TODO
 *
 * @param object $order
 *   TODO
 * @param string $key
 *   TODO
 */
function _giroconnect_uc_cart_process_value(&$order, $key) {
  $value = empty($_POST['panes']['payment']['details'][$key]) ? '' : check_plain($_POST['panes']['payment']['details'][$key]);
  $order->payment_details[$key] = $value;
}

/**
 * TODO
 *
 * @param string $pm
 *   TODO
 * @param object $order
 *   TODO
 *
 * @return array
 *   TODO
 */
function _giroconnect_uc_cart_review($pm, $order) {
  $review = array();
  switch ($pm) {
    case GIROCONNECT_METHOD_GIROPAY:
      if (!empty($order->payment_details['giroconnect_giropay_bankcode'])) {
        $review[] = array('title' => GiroConnectTools::getText('bank code'), 'data' => check_plain($order->payment_details['giroconnect_giropay_bankcode']));
      }
      break;

    case GIROCONNECT_METHOD_GIROMOBILE:
      break;

    case GIROCONNECT_METHOD_GIROTRUST:
      break;

    case GIROCONNECT_METHOD_DIRECTDEBIT:
      if (!empty($order->payment_details['giroconnect_directdebit_bankcode'])) {
        $review[] = array('title' => GiroConnectTools::getText('bank code'), 'data' => check_plain($order->payment_details['giroconnect_directdebit_bankcode']));
      }
      if (!empty($order->payment_details['giroconnect_directdebit_bankaccount'])) {
        $review[] = array('title' => GiroConnectTools::getText('bank account number'), 'data' => check_plain($order->payment_details['giroconnect_directdebit_bankaccount']));
      }
      break;

    case GIROCONNECT_METHOD_CREDITCARD:
      $lib = new GiroConnectCreditcard();
      if (!empty($order->payment_details['giroconnect_creditcard_cardtype'])) {
        $review[] = array('title' => GiroConnectTools::getText('creditcard types'), 'data' => $lib->getCardType(check_plain($order->payment_details['giroconnect_creditcard_cardtype'])));
      }
      break;

  }
  return $review;
}

/**
 * TODO
 *
 * @param object $order
 *   TODO
 */
function _giroconnect_uc_save_details($order) {
  // Load up the existing data array.
  $data = db_select('uc_orders', 'o')
    ->fields('o', array('data'))
    ->condition('o.order_id', $order->order_id)
    ->execute()
    ->fetchField();
  if (!empty($data)) {
    $data = unserialize($data);
  }
  switch ($order->payment_method) {
    case GIROCONNECT_METHOD_GIROPAY:
      $data['giroconnect_giropay_bankcode'] = $order->payment_details['giroconnect_giropay_bankcode'];
      break;

    case GIROCONNECT_METHOD_GIROMOBILE:
      break;

    case GIROCONNECT_METHOD_GIROTRUST:
      break;

    case GIROCONNECT_METHOD_DIRECTDEBIT:
      $data['giroconnect_directdebit_bankcode'] = $order->payment_details['giroconnect_directdebit_bankcode'];
      $data['giroconnect_directdebit_bankaccount'] = $order->payment_details['giroconnect_directdebit_bankaccount'];
      break;

    case GIROCONNECT_METHOD_CREDITCARD:
      $data['giroconnect_creditcard_cardtype'] = $order->payment_details['giroconnect_creditcard_cardtype'];
      break;

  }
  db_update('uc_orders')
    ->fields(array('data' => serialize($data)))
    ->condition('order_id', $order->order_id)
    ->execute();
}

/**
 * TODO
 *
 * @param object $order
 *   TODO
 */
function _giroconnect_uc_load_details(&$order) {
  // Load up the existing data array.
  $data = db_select('uc_orders', 'o')
    ->fields('o', array('data'))
    ->condition('o.order_id', $order->order_id)
    ->execute()
    ->fetchField();
  if (empty($data)) {
    return;
  }
  $data = unserialize($data);
  switch ($order->payment_method) {
    case GIROCONNECT_METHOD_GIROPAY:
      $order->payment_details['giroconnect_giropay_bankcode'] = $data['giroconnect_giropay_bankcode'];
      break;

    case GIROCONNECT_METHOD_GIROMOBILE:
      break;

    case GIROCONNECT_METHOD_GIROTRUST:
      break;

    case GIROCONNECT_METHOD_DIRECTDEBIT:
      $order->payment_details['giroconnect_directdebit_bankcode'] = $data['giroconnect_directdebit_bankcode'];
      $order->payment_details['giroconnect_directdebit_bankaccount'] = $data['giroconnect_directdebit_bankaccount'];
      break;

    case GIROCONNECT_METHOD_CREDITCARD:
      $order->payment_details['giroconnect_creditcard_cardtype'] = $data['giroconnect_creditcard_cardtype'];
      break;

  }
}

function _giroconnect_uc_start_payment($order) {
  $lib = _giroconnect_init_library($order->payment_method);
  $amount = uc_currency_format($order->order_total, FALSE, FALSE, ',');
  $currency = 'EUR';
  $text = GiroConnectTools::getText('order number') . ' ' . $order->order_id . '-' . $order->uid;
  switch ($order->payment_method) {
    case GIROCONNECT_METHOD_GIROPAY:
      $result = $lib->startTransaction(
        $amount,
        $currency,
        $order->order_id,
        $text,
        $order->payment_details['giroconnect_giropay_bankcode']
      );
      break;

    case GIROCONNECT_METHOD_GIROMOBILE:
      $result = $lib->startTransaction(
        $amount,
        $currency,
        $order->order_id,
        $text
      );
      break;

    case GIROCONNECT_METHOD_GIROTRUST:
      $result = $lib->startTransaction(
        $amount,
        $currency,
        $order->order_id,
        variable_get(GIROCONNECT_METHOD_GIROTRUST . '_feesplit', '50')
      );
      break;

    case GIROCONNECT_METHOD_DIRECTDEBIT:
      $result = $lib->startTransaction(
        $amount,
        $currency,
        $order->order_id,
        $text,
        $order->payment_details['giroconnect_directdebit_bankcode'],
        $order->payment_details['giroconnect_directdebit_bankaccount']
      );
      break;

    case GIROCONNECT_METHOD_CREDITCARD:
      $result = $lib->startTransaction(
        $amount,
        $currency,
        $order->order_id,
        $text,
        $order->payment_details['giroconnect_creditcard_cardtype'],
        variable_get(GIROCONNECT_METHOD_CREDITCARD . '_3dsecure', FALSE),
        variable_get(GIROCONNECT_METHOD_CREDITCARD . '_paymode', 'payonly')
      );
      break;

  }

  if ($result['status'] == GIROCONNECT_STATUS_OK) {
    unset($_SESSION['uc_checkout'][$order->order_id]['do_review']);
    $_SESSION['giroconnect'] = array(
      'order_id' => $order->order_id,
      '#markup' => $result['redirect']['#markup'],
    );
    drupal_goto('admin/giroconnect/uc_payment');
  }
  return array(
    array(
      'pass' => FALSE,
      'message' => GiroConnectTools::getText('gateway unavailable'),
    )
  );
}

function giroconnect_uc_payment() {
  if (empty($_SESSION['giroconnect']['order_id'])) {
    drupal_access_denied();
  }
  $order = uc_order_load($_SESSION['giroconnect']['order_id']);
  unset($_SESSION['giroconnect']['order_id']);
  if (empty($order)) {
    drupal_access_denied();
  }
  if (empty($_SESSION['giroconnect']['#markup'])) {
    drupal_access_denied();
  }
  $markup = $_SESSION['giroconnect']['#markup'];
  unset($_SESSION['giroconnect']['#markup']);
  return $markup;
}
/**
 * TODO
 */
function giroconnect_ubercart_completed() {
  if (empty($_GET['pm'])) {
    drupal_access_denied();
  }
  $pm = $_GET['pm'];
  $lib = _giroconnect_init_library($pm);
  $data = $lib->checkHash();
  if (empty($data['verified'])) {
    watchdog('giroconnect', 'Received redirect with invalid GET data: %var', array('%var' => print_r($data, TRUE)), WATCHDOG_ERROR);
    $gp_code = 0;
  }
  else {
    watchdog('giroconnect', 'Received valid redirect with GET data: %var', array('%var' => print_r($data, TRUE)), WATCHDOG_INFO);
    $order_id = $data['transactionId'];
    if (intval($_SESSION['cart_order']) != $order_id) {
      $_SESSION['cart_order'] = $order_id;
    }
    if (!($order = uc_order_load($order_id))) {
      drupal_goto('cart');
    }
    $gp_code = $data['gpCode'];
  }
  $text = GiroConnectTools::getText('gpCode' . $gp_code, $pm);
  if (isset($lib) && $lib->paymentSuccess($gp_code)) {
    $_SESSION['uc_checkout'][$_SESSION['cart_order']]['do_complete'] = TRUE;
    drupal_set_message($text, 'status');
    drupal_goto('cart/checkout/complete');
  }
  watchdog('giroconnect', $text, array(), WATCHDOG_ERROR);
  drupal_set_message($text, 'warning');
  drupal_goto('cart/checkout');
}

/**
 * TODO
 */
function giroconnect_ubercart_notify() {
  $lib = _giroconnect_init_library($_GET['pm']);
  $data = $lib->checkHash();
  $result = '400';
  if (!$data['verified']) {
    watchdog('giroconnect', 'Received notification with invalid GET data: %var', array('%var' => print_r($data, TRUE)), WATCHDOG_ERROR);
  }
  else {
    watchdog('giroconnect', 'Received valid notification with GET data: %var', array('%var' => print_r($data, TRUE)), WATCHDOG_INFO);
    $order_id = $data['transactionId'];
    $order = uc_order_load($order_id);
    if ($order == FALSE) {
      watchdog('giroconnect', 'Notification attempted for non-existent order.', array(), WATCHDOG_ERROR);
    }
    else {
      $gp_code = $data['gpCode'];
      if ($lib->paymentSuccess($gp_code)) {
        $amount = str_replace(',', '.', check_plain($_GET['amount']));
        $comment = $lib->paymentMethod() . ' transaction';
        uc_payment_enter($order_id, $lib->paymentMethod(), $amount, $order->uid, NULL, $comment);
        $text = t('@method notified about payment of @amount @currency.', array(
          '@method' => $lib->paymentMethod(),
          '@amount' => $amount,
          '@currency' => 'EUR',
        ));
        uc_order_comment_save($order_id, 0, $text, 'order', 'payment_received');
      }
      $result = '200';
    }
  }
  header('http_code: ' . $result, TRUE, $result);
  print $result;
  exit;
}
