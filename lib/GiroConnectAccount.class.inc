<?php
/**
 * This file holds classes for contact informations transfered by
 * the GiroMobile login integration.
 *
 * @package GiroConnect
 */

/**
 * Description of GiroConnectAccount
 *
 * Constructed following http://xml.coverpages.org/xnal.html
 *
 * @package GiroConnect
 */
class GiroConnectAccount {

  private $person;
  private $addresses;

  public function __construct($account = NULL) {
    if (!empty($account) && is_string($account)) {
      $account = json_decode($account);
    }
    $this->person = empty($account->person) ? new GiroConnectAccountPerson() : new GiroConnectAccountPerson($account->person);
    $this->addresses = array();
    if (empty($account->addresses)) {
      $this->addresses[] = new GiroConnectAccountAddress();
    }
    else {
      foreach ($account->addresses as $address) {
        $this->addresses[] = new GiroConnectAccountAddress($address);
      }
    }
  }

  public function __toString() {
    return GiroConnectTools::_json_encode($this->_toObject());
  }

  private function _toObject() {
    $account = new stdClass();
    $account->person = (string)$this->person;
    $account->addresses = array();
    foreach ($this->addresses as $address) {
      $account->addresses[] = (string)$address;
    }
    return $account;
  }

  /**
   * TODO
   *
   * @return GiroConnectAccountPerson
   *   TODO
   */
  public function person() {
    return $this->person;
  }

  /**
   * TODO
   *
   * @return GiroConnectAccountAddress
   *   TODO
   */
  public function deliveryAddress() {
    foreach ($this->addresses as $address) {
      if ($address->isForDelivery()) {
        return $address;
      }
    }
    return FALSE;
  }

  /**
   * TODO
   *
   * @return GiroConnectAccountAddress
   *   TODO
   */
  public function invoiceAddress() {
    foreach ($this->addresses as $address) {
      if ($address->isForInvoice()) {
        return $address;
      }
    }
    return FALSE;
  }

}

/**
 * Description of GiroConnectAccountPerson
 *
 * @package GiroConnect
 */
class GiroConnectAccountPerson {

  private $PrecedingTitle;
  private $Title;
  private $FirstName;
  private $MiddleName;
  private $LastNamePrefix;
  private $LastName;
  private $OtherName;
  private $FormerName;
  private $Alias;
  private $GenerationIdentifier;
  private $Suffix;
  private $GeneralSuffix;
  private $Function;

  private $Gender;
  private $DayOfBirth;
  private $YearOfBirth;

  public function __construct($person = NULL) {
    if (!empty($person) && is_string($person)) {
      $person = json_decode($person);
    }
    $this->PrecedingTitle       = empty($person->PrecedingTitle)      ? '' : $person->PrecedingTitle;
    $this->Title                = empty($person->Title)               ? '' : $person->Title;
    $this->FirstName            = empty($person->FirstName)           ? '' : $person->FirstName;
    $this->MiddleName           = empty($person->MiddleName)          ? '' : $person->MiddleName;
    $this->LastNamePrefix       = empty($person->LastNamePrefix)      ? '' : $person->LastNamePrefix;
    $this->LastName             = empty($person->LastName)            ? '' : $person->LastName;
    $this->OtherName            = empty($person->OtherName)           ? '' : $person->OtherName;
    $this->FormerName           = empty($person->FormerName)          ? '' : $person->FormerName;
    $this->Alias                = empty($person->Alias)               ? '' : $person->Alias;
    $this->GenerationIdentifier = empty($person->GenerationIdentifier)? '' : $person->GenerationIdentifier;
    $this->Suffix               = empty($person->Suffix)              ? '' : $person->Suffix;
    $this->GeneralSuffix        = empty($person->GeneralSuffix)       ? '' : $person->GeneralSuffix;
    $this->Function             = empty($person->Function)            ? '' : $person->Function;
    $this->Gender               = empty($person->Gender)              ? GIROCONNECT_GENDER_UNKNOWN : $person->Gender;
    $this->DayOfBirth           = empty($person->DayOfBirth)          ? '' : $person->DayOfBirth;
    $this->YearOfBirth          = empty($person->YearOfBirth)         ? '' : $person->YearOfBirth;
  }

  public function __set($name, $value) {
    $this->{$name} = $value;
  }

  public function __get($name) {
    return $this->{$name};
  }

  public function __toString() {
    return GiroConnectTools::_json_encode($this->_toObject());
  }

  private function _toObject() {
    $person = array(
      'PrecedingTitle'      => $this->PrecedingTitle,
      'Title'               => $this->Title,
      'FirstName'           => $this->FirstName,
      'MiddleName'          => $this->MiddleName,
      'LastNamePrefix'      => $this->LastNamePrefix,
      'LastName'            => $this->LastName,
      'OtherName'           => $this->OtherName,
      'FormerName'          => $this->FormerName,
      'Alias'               => $this->Alias,
      'GenerationIdentifier'=> $this->GenerationIdentifier,
      'Suffix'              => $this->Suffix,
      'GeneralSuffix'       => $this->GeneralSuffix,
      'Function'            => $this->Function,
      'Gender'              => $this->Gender,
      'DayOfBirth'          => $this->DayOfBirth,
      'YearOfBirth'         => $this->YearOfBirth,
    );
    return (object)$person;
  }

}

/**
 * Description of GiroConnectAccountAddress
 *
 * @package GiroConnect
 */
class GiroConnectAccountAddress {

  private $DeliveryIdentifier;
  private $Country;
  private $AdministrativeArea;
  private $SubAdministrativeArea;
  private $Locality;
  private $DependentLocality;
  private $PostalCode;
  private $Thoroughfare;
  private $Premise;
  private $SubPremise;
  private $OrganisationName;

  private $forDelivery;
  private $forInvoice;

  public function __construct($address = NULL) {
    if (!empty($address) && is_string($address)) {
      $address = json_decode($address);
    }
    $this->DeliveryIdentifier   = empty($address->DeliveryIdentifier)   ? '' : $address->DeliveryIdentifier;
    $this->Country              = empty($address->Country)              ? '' : $address->Country;
    $this->AdministrativeArea   = empty($address->AdministrativeArea)   ? '' : $address->AdministrativeArea;
    $this->SubAdministrativeArea= empty($address->SubAdministrativeArea)? '' : $address->SubAdministrativeArea;
    $this->Locality             = empty($address->Locality)             ? '' : $address->Locality;
    $this->DependentLocality    = empty($address->DependentLocality)    ? '' : $address->DependentLocality;
    $this->PostalCode           = empty($address->PostalCode)           ? '' : $address->PostalCode;
    $this->Thoroughfare         = empty($address->Thoroughfare)         ? '' : $address->Thoroughfare;
    $this->Premise              = empty($address->Premise)              ? '' : $address->Premise;
    $this->SubPremise           = empty($address->SubPremise)           ? '' : $address->SubPremise;
    $this->OrganisationName     = empty($address->OrganisationName)     ? '' : $address->OrganisationName;
    $this->forDelivery          = empty($address->forDelivery)        ? TRUE : $address->forDelivery;
    $this->forInvoice           = empty($address->forInvoice)         ? TRUE : $address->forInvoice;
  }

  public function __set($name, $value) {
    $this->{$name} = $value;
  }

  public function __get($name) {
    return $this->{$name};
  }

  public function __toString() {
    return GiroConnectTools::_json_encode($this->_toObject());
  }

  private function _toObject() {
    $address = array(
      'DeliveryIdentifier'    => $this->DeliveryIdentifier,
      'Country'               => $this->Country,
      'AdministrativeArea'    => $this->AdministrativeArea,
      'SubAdministrativeArea' => $this->SubAdministrativeArea,
      'Locality'              => $this->Locality,
      'DependentLocality'     => $this->DependentLocality,
      'PostalCode'            => $this->PostalCode,
      'Thoroughfare'          => $this->Thoroughfare,
      'Premise'               => $this->Premise,
      'SubPremise'            => $this->SubPremise,
      'OrganisationName'      => $this->OrganisationName,
      'forDelivery'           => $this->forDelivery,
      'forInvoice'            => $this->forInvoice,
    );
    return (object)$address;
  }

  public function isForDelivery() {
    return $this->forDelivery;
  }

  public function isForInvoice() {
    return $this->forInvoice;
  }

}
