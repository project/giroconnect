<?php
/**
 * This file contains class GiroConnectTools with general resources for the whole library.
 *
 * @package GiroConnect
 */

/**
 * Provides some global functions for the GiroConnect SDK
 *
 * All functions in this class are static and can be called directly.
 *
 * @package GiroConnect
 */
class GiroConnectTools {

  /**
   * Get some informations about the environment.
   *
   * Informations about the Server protocol, base directory and
   * paths.
   *
   * @return array
   *   Array of useful parameters about the environment.
   *   [
   *   is_https => is https,
   *   ]
   */
  static function getBaseInfo() {
    $is_https = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on';
    $http_protocol = $is_https ? 'https' : 'http';
    $base_root = $http_protocol . '://' . $_SERVER['HTTP_HOST'];
    $base_url = $base_root;
    if ($dir = rtrim(dirname($_SERVER['SCRIPT_NAME']), '\/')) {
      $base_path = $dir;
      $base_url .= $base_path;
      $base_path .= '/';
    }
    else {
      $base_path = '/';
    }
    $path = substr(dirname(__FILE__), strlen($_SERVER['DOCUMENT_ROOT']));
    // Replace backslashes with slashes, just in case we are on a Windows box.
    $lib_path = str_replace("\\", "/", $path);
    // Remove beginning slash.
    if (substr($lib_path, 0, 1) == '/') {
      $lib_path = substr($lib_path, 1);
    }

    return array(
      'is_https'      => $is_https,
      'http_protocol' => $http_protocol,
      'base_root'     => $base_root,
      'base_url'      => $base_url,
      'base_path'     => $base_path,
      'lib_path'      => $lib_path,
    );
  }

  /**
   * @static
   * @param $id
   * @param array $param
   * @return string
   */
  static function getText($id, $pm = 'default', $param = array(), $language = 'de') {
    $t = self::allText($pm, $language);
    $id = isset($t[$id]) ? $id : 'unknown';

    if (isset($t[$id])) {
      $text = $t[$id];
    }
    else {
      $text = '[undefined text key]';
    }

    return sprintf($text, $param);
  }

  /**
   * @static
   * @return string
   */
  static function getCSS() {
    return '<style type="text/css">
      #giroconnect-payment-info .label {
        font-weight: bold;
        display: inline-block;
        width: 170px;
      }
      #giroconnect-payment-info .label:after {
        content: ":";
      }
    </style>';
  }

  /**
   * TODO
   *
   * @return array
   *   TODO
   */
  static function paymentMethods() {
    return array(
      GIROCONNECT_METHOD_GIROPAY,
      GIROCONNECT_METHOD_GIROMOBILE,
      GIROCONNECT_METHOD_GIROTRUST,
      GIROCONNECT_METHOD_DIRECTDEBIT,
      GIROCONNECT_METHOD_CREDITCARD,
    );
  }

  /**
   * @static
   * @param $data
   * @return string
   */
  static function hash($data, $secret) {
    $hashdataImplode = implode('', $data);

    // NOTE: We do not need this anymore because we removed check_plain() at the other end.
    // Sanitize the hashdata.
    // $hashdataImplode = str_replace('&', '&amp;', $hashdataImplode);

    // hash_hmac Funktion ist erst seit PHP Version 5.1.2 verfügbar
    if (function_exists('hash_hmac')) {
      return hash_hmac('md5', $hashdataImplode, $secret);
    }

    // Fallback Implementierung für PHP4
    $b = 64; // byte length for md5
    if (strlen($secret) > $b) {
      $secret = pack("H*", md5($secret));
    }
    $secret = str_pad($secret, $b, chr(0x00));
    $ipad = str_pad('', $b, chr(0x36));
    $opad = str_pad('', $b, chr(0x5c));
    $k_ipad = $secret ^ $ipad;
    $k_opad = $secret ^ $opad;
    return md5($k_opad . pack("H*", md5($hashdataImplode . $data)));
  }

  static function buildForm($def) {
    $form = empty($def['#markup']) ? '' : $def['#markup'];
    $form .= '<form method="POST" action="' . $def['action'] . '">';
    foreach ($def['values'] as $key => $value) {
      $form .= '<input type="hidden" name="' . $key . '" value="' . $value . '">';
    }
    $form .= '<input type="submit" value="' . $def['submit'] . '"></form>';
    return $form;
  }

  static function _json_output($var = NULL) {
    // We are returning JSON, so tell the browser.
    header('content-type: application/json');

    if (isset($var)) {
      echo GiroConnectTools::_json_encode($var);
    }
  }

  static function _json_encode($var) {
    // The PHP version cannot change within a request.
    static $php530;

    if (!isset($php530)) {
      $php530 = version_compare(PHP_VERSION, '5.3.0', '>=');
    }

    if ($php530) {
      // Encode <, >, ', &, and " using the json_encode() options parameter.
      return json_encode($var, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT);
    }

    // json_encode() escapes <, >, ', &, and " using its options parameter, but
    // does not support this parameter prior to PHP 5.3.0.  Use a helper instead.
    return GiroConnectTools::_json_encode_helper($var);
  }

  static function _json_encode_helper($var) {
    switch (gettype($var)) {
      case 'boolean':
        return $var ? 'true' : 'false'; // Lowercase necessary!

      case 'integer':
      case 'double':
        return $var;

      case 'resource':
      case 'string':
        // Always use Unicode escape sequences (\u0022) over JSON escape
        // sequences (\") to prevent browsers interpreting these as
        // special characters.
        $replace_pairs = array(
          // ", \ and U+0000 - U+001F must be escaped according to RFC 4627.
          '\\' => '\u005C',
          '"' => '\u0022',
          "\x00" => '\u0000',
          "\x01" => '\u0001',
          "\x02" => '\u0002',
          "\x03" => '\u0003',
          "\x04" => '\u0004',
          "\x05" => '\u0005',
          "\x06" => '\u0006',
          "\x07" => '\u0007',
          "\x08" => '\u0008',
          "\x09" => '\u0009',
          "\x0a" => '\u000A',
          "\x0b" => '\u000B',
          "\x0c" => '\u000C',
          "\x0d" => '\u000D',
          "\x0e" => '\u000E',
          "\x0f" => '\u000F',
          "\x10" => '\u0010',
          "\x11" => '\u0011',
          "\x12" => '\u0012',
          "\x13" => '\u0013',
          "\x14" => '\u0014',
          "\x15" => '\u0015',
          "\x16" => '\u0016',
          "\x17" => '\u0017',
          "\x18" => '\u0018',
          "\x19" => '\u0019',
          "\x1a" => '\u001A',
          "\x1b" => '\u001B',
          "\x1c" => '\u001C',
          "\x1d" => '\u001D',
          "\x1e" => '\u001E',
          "\x1f" => '\u001F',
          // Prevent browsers from interpreting these as as special.
          "'" => '\u0027',
          '<' => '\u003C',
          '>' => '\u003E',
          '&' => '\u0026',
          // Prevent browsers from interpreting the solidus as special and
          // non-compliant JSON parsers from interpreting // as a comment.
          '/' => '\u002F',
          // While these are allowed unescaped according to ECMA-262, section
          // 15.12.2, they cause problems in some JSON parsers.
          "\xe2\x80\xa8" => '\u2028', // U+2028, Line Separator.
          "\xe2\x80\xa9" => '\u2029', // U+2029, Paragraph Separator.
        );

        return '"' . strtr($var, $replace_pairs) . '"';

      case 'array':
        // Arrays in JSON can't be associative. If the array is empty or if it
        // has sequential whole number keys starting with 0, it's not associative
        // so we can go ahead and convert it as an array.
        if (empty($var) || array_keys($var) === range(0, sizeof($var) - 1)) {
          $output = array();
          foreach ($var as $v) {
            $output[] = GiroConnectTools::_json_encode_helper($v);
          }
          return '[ ' . implode(', ', $output) . ' ]';
        }
        // Otherwise, fall through to convert the array as an object.

      case 'object':
        $output = array();
        foreach ($var as $k => $v) {
          $output[] = GiroConnectTools::_json_encode_helper(strval($k)) . ':' . GiroConnectTools::_json_encode_helper($v);
        }
        return '{' . implode(', ', $output) . '}';

      default:
        return 'null';
    }
  }

  static function _url($path, $data) {
    $query = '';
    $sep = (strpos($path, '?') === FALSE) ? '?' : '&';
    foreach ($data as $key => $value) {
      $query .= $sep . $key . '=' . urlencode($value);
      $sep = '&';
    }
    return GIROCONNECT_SERVER . '/' . $path . $query;
  }

  static function getUrl($url, $params) {
    $query = '';
    $sep = (strpos($url, '?') === FALSE) ? '?' : '&';
    foreach ($params as $key => $value) {
      $query .= $sep . $key . '=' . urlencode($value);
      $sep = '&';
    }

    return $url . $query;
  }

  static function httpRequest($url, $params = array(), $post = FALSE) {
    if (!$post) {
      $url = self::getUrl($url, $params);
    }
    $ch = curl_init($url);
    if ($post) {
      curl_setopt($ch, CURLOPT_POST, TRUE);
      curl_setopt($ch, CURLOPT_HTTPGET, FALSE);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    }
    else {
      curl_setopt($ch, CURLOPT_POST, FALSE);
      curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
    }

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);  // DO NOT RETURN HTTP HEADERS
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);  // RETURN THE CONTENTS OF THE CALL
    $result = curl_exec($ch);
    curl_close($ch);
    if (is_string($result)) {
      $chr = substr($result, 0, 1);
      if ($chr == '{' || $chr == '"') {
        $result = json_decode($result);
      }
      else {
        if ($chr == '<') {
          $result = array(
            'error'     => TRUE,
            'error_msg' => self::getText('http error'),
          );
        }
      }
    }

    if (is_object($result)) {
      $result = (array) $result;
    }
    if (empty($result) || (is_array($result) && !empty($result['error']))) {
      $result['status'] = GIROCONNECT_STATUS_CANCEL;
    }
    return $result;
  }

  private static function outputArray(&$output, $values) {
    foreach ($values as $key => $value) {
      $output .= "'$key'=>";
      if (is_array($value) || is_object($value)) {
        $output .= 'array(';
        self::outputArray($output, $value);
        $output .= '),';
      }
      else {
        $output .= "'$value',";
      }
    }
  }

  public static function loadLocalLanguageFile($language) {
    return self::loadLanguageFile($language, TRUE);
  }

  private static function loadLanguageFile($language, $forceLocal = FALSE) {
    $file = FALSE;
    if (!$forceLocal && function_exists('sys_get_temp_dir') && ($_SERVER['HTTP_HOST'] != GIROCONNECT_PSP_HOST)) {
      $dir = sys_get_temp_dir();
      if (is_dir($dir) && is_writable($dir)) {
        $file = $dir . DIRECTORY_SEPARATOR . 'giroconnect.lng.' . $language . '.php';
        if (!file_exists($file) || (time() - filectime($file) > 604800)) {
          // Try to refresh the lanaguage strings if not yet present or older than a week.
          $result = self::httpRequest(GIROCONNECT_SERVER . '/giroconnect/language/' . $language);
          if (!empty($result['error'])) {
            $file = FALSE;
          }
          else {
            $output = '<?php $strings=array(';
            self::outputArray($output, $result);
            $output .= ');';
            if (!file_put_contents($file, $output)) {
              $file = FALSE;
            }
          }
        }
      }
    }
    if (!$file) {
      $file = dirname(__FILE__) . '/languages/' . $language . '.inc';
    }
    $strings = array();
    include $file;
    return $strings;
  }

  private static function allText($pm = 'default', $language = 'de') {
    static $t;
    if (!isset($t)) {
      $t = array();
    }
    if (empty($t[$language])) {
      $t[$language] = self::loadLanguageFile($language);
    }
    if (!isset($t[$language][$pm])) {
      $t[$language][$pm] = $t[$language]['default'];
    }
    return $t[$language][$pm];
  }
}
