<?php
/**
 * This file contains the GiroConnect subclass for credit card transactions.
 *
 * @package GiroConnect
 */

/**
 * Description for class GiroConnectCreditcard
 *
 * Data for test transactions:
 * Visa 4035874000424977, valid until 10/2014, PW: secret3
 * Mastercard 5426064000424979, valid until 10/2014, PW: secret3
 *
 * @package GiroConnect
 */
class GiroConnectCreditcard extends GiroConnectAbstract {
  protected $pm = GIROCONNECT_METHOD_CREDITCARD;
  protected $paymentFields = array(
    'sourceId' => TRUE,
    'merchantId' => TRUE,
    'projectId' => TRUE,
    'transactionId' => TRUE,
    'amount' => TRUE,
    'currency' => 'EUR',
    'vwz' => TRUE,
    'ccType' => TRUE,
    '3dSecure' => TRUE,
    'paymode' => TRUE,
    'urlRedirect' => TRUE,
    'urlNotify' => TRUE,
  );
  private $cardTypes = array(
    'M' => 'MasterCard',
    'V' => 'Visa',
    'A' => 'American Express',
    'C' => 'Diners',
    'J' => 'JCB',
  );
  private $payModes = array();

  public function __construct($mid = '', $pid = '', $secret = '', $sourceId = GIROCONNECT_SOURCE_ID_TESTMODE) {
    parent::__construct($mid, $pid, $secret, $sourceId);
    $this->payModes = array(
      'payonly' => GiroConnectTools::getText('pay mode:payonly', GIROCONNECT_METHOD_CREDITCARD),
      'payplus' => GiroConnectTools::getText('pay mode:payplus', GIROCONNECT_METHOD_CREDITCARD),
      'fullpay' => GiroConnectTools::getText('pay mode:fullpay', GIROCONNECT_METHOD_CREDITCARD),
    );
  }

  /**
   * Start a credit card transaction
   *
   * @param float $amount
   *   The amount to be paid with comma as a decimal separator.
   * @param string $transactionId
   *   The unique transaction id to be displayed in the GiroCockpit of the merchant and used as a reference for future
   *   activities, e.g. charge back.
   * @param string $text
   *   Additional text as a memo for the transaction.
   * @param string $ccType
   *   The selected type of the credit card used for transaction. Possible values @see $this->cardTypes
   * @param bool $secure
   *   If 3D Secure is enabled for your account but either the security gateway is not available of the credit card
   *   does not support the security feature, this parameter defines whether you accept also insecure transactions.
   * @param string $mode
   *   Determines how much data will be collected from the buyer during processing. Possible values @see $this->payModes
   *
   * @return array
   *   TODO
   */
  public function startTransaction($amount, $currency, $transactionId, $text, $ccType, $secure = TRUE, $mode = 'payonly') {
    if (!array_key_exists($ccType, $this->cardTypes) || !array_key_exists($mode, $this->payModes)) {
      return array(
        'status' => GIROCONNECT_STATUS_CANCEL,
        'error' => TRUE,
        'error_msg' => 'Incorrect parameters given to function GiroConnectCreditcard::startTransactions().',
      );
    }
    $this->paymentParams['amount'] = $amount;
    $this->paymentParams['currency'] = $currency;
    $this->paymentParams['transactionId'] = $transactionId;
    $this->paymentParams['vwz'] = trim(substr($text, 0, 27));
    $this->paymentParams['ccType'] = $ccType;
    $this->paymentParams['3dSecure'] = (int) $secure;
    $this->paymentParams['paymode'] = $mode;

    return $this->getTransactionUrl();
  }

  public function getCardTypes() {
    return $this->cardTypes;
  }

  public function getCardType($type) {
    return $this->cardTypes[$type];
  }

  public function getPayModes() {
    return $this->payModes;
  }

  public function getFormFieldCardType($enabled = array('M', 'V', 'A', 'C', 'J')) {
    $output = '<div class="form-item form-type-select">';
    $output .= '<label for="giroconnect-cardtype">' . GiroConnectTools::getText('ccType:FieldLabel', GIROCONNECT_METHOD_CREDITCARD) . '</label>';
    $output .= '<select name="giroconnect-cardtype" id="giroconnect-cardtype" class="giroconnect-cardtype form-select">';
    $selected = ' selected="selected"';
    foreach ($this->cardTypes as $key => $label) {
      if (!in_array($key, $enabled)) {
        continue;
      }
      $output .= '<option' . $selected . ' value="' . $key . '">' . $label . '</option>';
      $selected = '';
    }
    $output .= '</select>';
    $output .= '<div class="description">' . GiroConnectTools::getText('ccType:FieldDescription', GIROCONNECT_METHOD_CREDITCARD) . '</div>';
    $output .= '</div>';
    return $output;
  }

}
