/*
 * GiroConnect jQuery plugin v0.1
 * https://www.girosolution.de
 * Copyright 2012, GiroSolution AG, https://www.girosolution.de
 */
var GiroConnect = GiroConnect || { 'settings': {} };
// jQuery.noConflict(); is needed for WebSites that use Scriptaculous or Prototype JavaScript Libraries,
// because they also use the $ Variable. Magento for example is using Scriptaculous.
// See also (http://api.jquery.com/jQuery.noConflict/)
jQuery.noConflict();

(function($) {

GiroConnect = GiroConnect || {};
GiroConnect.settings = GiroConnect.settings || {};

GiroConnect.init = function() {
  $('body').addClass('giromobile');
  $('.giromobile-widget .tid').click(function() {return false;});
  $('#giromobile-login').click(function() {GiroConnect.start(this, 'Login');});
  $('#giromobile-register').click(function() {GiroConnect.start(this, 'Register');});
  $('#giromobile-pay.with-click').click(function() {
    var params = new Object();
    params['oid'] = $(this).attr('oid');
    params['amount'] = $(this).attr('amount');
    params['currency'] = $(this).attr('currency');
    GiroConnect.start(this, 'Pay', params);
  });
  $('#giromobile-pay.without-click').each(function() {
    GiroConnect.settings.mode = GiroConnect.settings.extra.mode;
    GiroConnect.showTid(this, GiroConnect.settings.extra.tid, GiroConnect.settings.extra.img);
  });
};

GiroConnect.showTid = function(el, tid, img) {
  $(el).addClass('active');
  GiroConnect.settings.tid = tid;
  GiroConnect.settings.placeholder = $('.tid', el).html();
  $('.tid', el).html('<img src="' + img + '">');
  GiroConnect.settings.queue = setInterval(GiroConnect.poll, 2000);
};

GiroConnect.start = function(el, mode, params) {
  $(el).addClass('active');
  GiroConnect.settings.mode = mode;
  var extra = '';
  if (params !== undefined) {
    for (var key in params) {
      extra = extra + '&' + key + '=' + params[key];
    }
  }
  $.ajax({
    url: GiroConnect.settings.urlAjax + '&action=gettid&mode=' + GiroConnect.settings.mode + extra,
    processData: false,
    async: true,
    dataType: 'json',
    success: function (data, textStatus) {
      if (data.status == GiroConnect.settings.status.ok) {
        GiroConnect.showTid(el, data.redirect.tid, data.redirect.img);
      }
      else {
        $(el).removeClass('active');
      }
    }
  });
};

GiroConnect.poll = function() {
  $.ajax({
    url: GiroConnect.settings.urlAjax + '&action=poll&mode=' + GiroConnect.settings.mode + '&tid=' + GiroConnect.settings.tid,
    processData: false,
    async: true,
    dataType: 'json',
    success: function (data, textStatus) {
      switch (data.status) {
        // Polling received a result.
        case GiroConnect.settings.status.ok:
          clearInterval(GiroConnect.settings.queue);
          $('#giromobileBackdrop').addClass('show');
          if (data.linktext !== undefined) {
            var text = '';
            if (data.text !== undefined) {
              text = data.text;
            }
            text = text + '<p><a href="' + data.url + '">' + data.linktext + '</a></p>';
            $('#giromobileBackdrop #giromobileMsg').html(text);
          }
          else {
            document.location = data.url;
          }
          break;
        // Polling received a cancel request.
        case GiroConnect.settings.status.cancel:
          clearInterval(GiroConnect.settings.queue);
          $('.giromobile-widget').removeClass('active');
          $('.giromobile-widget .tid').html(GiroConnect.settings.placeholder);
          break;
        default:
          break;
      }
    }
  });
};

$(function () {
  GiroConnect.init();
});

})(jQuery);