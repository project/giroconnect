<?php
/**
 * This file contains the GiroConnect subclass for GiroMobile transactions.
 *
 * @package GiroConnect
 */

/**
 * Description for class GiroConnectGiromobile
 *
 * @package GiroConnect
 */
class GiroConnectGiromobile extends GiroConnectAbstract {
  protected $pm = GIROCONNECT_METHOD_GIROMOBILE;
  protected $paymentFields = array(
    'sourceId' => TRUE,
    'merchantId' => TRUE,
    'projectId' => TRUE,
    'transactionId' => TRUE,
    'amount' => 0,
    'currency' => 'EUR',
    'vwz' => FALSE,
    'tid' => TRUE,
    'timestamp' => TRUE,
    'transactionType' => TRUE,
    'urlRedirect' => TRUE,
    'urlNotify' => TRUE,
  );
  private $actions = array(
    'gettid' => 'giromobile/gettid',
    'poll' => 'giromobile/poll',
  );

  private $extra_settings = array();

  private $callback_js;
  private $callback_css;
  private $callback_login;
  private $callback_pay;

  function callbacks($prefix, $js, $css, $login, $pay) {
    $this->callback_js       = $prefix . '_' . $js;
    $this->callback_css      = $prefix . '_' . $css;
    $this->callback_login    = $prefix . '_' . $login;
    $this->callback_pay      = $prefix . '_' . $pay;
  }

  public function prepare($jqueryRequired = TRUE) {
    if ($jqueryRequired) {
      call_user_func($this->callback_js, '/' . $this->baseInfo['lib_path'] . '/js/jquery.js');
    }
    call_user_func($this->callback_js, '/' . $this->baseInfo['lib_path'] . '/js/giroconnect.js');
    call_user_func($this->callback_css, '/' . $this->baseInfo['lib_path'] . '/css/giroconnect.css');
  }

  public function settings() {
    return array(
      'urlAjax' => $this->baseInfo['base_url'] . '/' . $this->ajax_path,
      'status' => array(
        'continue' => GIROCONNECT_STATUS_CONTINUE,
        'ok' => GIROCONNECT_STATUS_OK,
        'cancel' => GIROCONNECT_STATUS_CANCEL,
      ),
      'extra' => $this->extra_settings,
    );
  }

  public function startTransaction($amount, $currency, $transactionId, $text) {
    $this->paymentParams['amount'] = $amount;
    $this->paymentParams['currency'] = $currency;
    $this->paymentParams['transactionId'] = $transactionId;
    $this->paymentParams['vwz'] = trim(substr($text, 0, 27));
    $this->paymentParams['tid'] = 0;
    $this->paymentParams['timestamp'] = $_SERVER['REQUEST_TIME'];
    $this->paymentParams['transactionType'] = 'Pay';

    $tid = $this->getTransactionUrl($this->actions['gettid']);
    if ($tid['status'] == GIROCONNECT_STATUS_OK) {
      $tid['#markup'] = $this->markupPay($transactionId, $amount, $currency, TRUE, $tid['redirect']['#markup']);
      $this->extra_settings['tid'] = $tid['redirect']['tid'];
      $this->extra_settings['img'] = $tid['redirect']['img'];
      $this->extra_settings['mode'] = 'Pay';
    }

    return $tid;
  }

  function ajax() {
    if (empty($_GET['action']) || empty($_GET['mode'])) {
      exit;
    }
    $this->is_ajax = TRUE;

    if (!empty($_GET['amount'])) {
      $this->paymentParams['amount'] = $_GET['amount'];
    }
    if (!empty($_GET['currency'])) {
      $this->paymentParams['currency'] = $_GET['currency'];
    }
    $this->paymentParams['transactionId'] = empty($_GET['oid']) ? 'n/a' : $_GET['oid'];
    $this->paymentParams['vwz'] = 'undefined';
    $this->paymentParams['tid'] = empty($_GET['tid']) ? 0 : $_GET['tid'];
    $this->paymentParams['timestamp'] = $_SERVER['REQUEST_TIME'];
    $this->paymentParams['transactionType'] = $_GET['mode'];

    $action = $_GET['action'];
    $result = $this->getTransactionUrl($this->actions[$action]);
    switch ($action) {
      case 'gettid':
        $output = $result;
        break;
      case 'poll':
        $output = $this->_poll($result);
        break;
      default:
        exit;
    }
    $this->ajaxOutput($output);
  }

  function ajaxOutput($output) {
    GiroConnectTools::_json_output($output);
    exit;
  }



  private function _poll($result) {
    if ($this->paymentParams['transactionType'] == 'Pay') {
      if ($result['status'] == GIROCONNECT_STATUS_CONTINUE) {
        return array(
          'status' => $result['status'],
        );
      }
      $url = call_user_func($this->callback_pay, $result['email'], $result['oid'], $result['amount'], $result['currency'], $result['paystate']);
      if ($url) {
        return array(
          'status' => GIROCONNECT_STATUS_OK,
          'url' => $url,
        );
      }
    }
    else {
      if ($result['status'] == GIROCONNECT_STATUS_OK) {
        $url = call_user_func($this->callback_login, $result['email'], $this->paymentParams['tid']);
        if ($url) {
          return array(
            'status' => GIROCONNECT_STATUS_OK,
            'url' => $url,
          );
        }
      }
      return array(
        'status' => $result['status'],
      );
    }
    return array(
      'status' => GIROCONNECT_STATUS_CANCEL,
    );
  }

  private function _get_hash($mode, $tid = 0) {
    $data = array(
      'sourceId' => $this->sourceId,
      'merchantId' => $this->mid,
      'projectId' => $this->pid,
      'transactionType' => $mode,
      'tid' => $tid,
      'timestamp' => $_SERVER['REQUEST_TIME'],
      'version' => GIROCONNECT_VERSION,
    );
    if (!empty($this->oid)) {
      $data['oid'] = $this->oid;
    }
    if (!empty($this->amount)) {
      $data['amount'] = $this->amount;
    }
    if (!empty($this->currency)) {
      $data['currency'] = $this->currency;
    }
    $data['hash'] = hash_hmac('md5', implode('', $data), $this->secret);
    return $data;
  }

  function accountDetails($email, GiroConnectAccount $account, $tid) {
    $data = $this->_get_hash('AccountDetails', $tid);
    $data['email'] = (string)$email;
    $data['details'] = (string)$account;

    if ($data = GiroConnectTools::httpRequest(GIROCONNECT_SERVER . '/giromobile/accountdetails', $data)) {
      return array(
        'status' => GIROCONNECT_STATUS_OK,
        'account' => new GiroConnectAccount($data),
      );
    }
    return array(
      'status' => GIROCONNECT_STATUS_CANCEL,
    );
  }

  private function _gettid($mode) {
    $data = $this->_get_hash($mode);

    if ($data = GiroConnectTools::httpRequest(GIROCONNECT_SERVER . '/giromobile/gettid', $data)) {
      return array(
        'status' => GIROCONNECT_STATUS_OK,
        'tid' => $data['tid'],
        'img' => $data['img'],
      );
    }
    return array(
      'status' => GIROCONNECT_STATUS_CANCEL,
    );
  }

  function markupLogin() {
    return '<div id="giromobile-login" class="giromobile-widget login">Login with GiroMobile<div class="tid">Please wait...</div></div>' . $this->_markupGeneral();
  }

  function markupRegister() {
    return '<div id="giromobile-register" class="giromobile-widget register">Register with GiroConnect<div class="tid">Please wait...</div></div>' . $this->_markupGeneral();
  }

  function markupPay($oid, $amount, $currency = 'EUR', $tid = FALSE, $extraMarkup = '') {
    if ($tid) {
      $class = "without-click";
    }
    else {
      $class = "with-click";
    }
    $content = 'Please wait...';
    return $extraMarkup . '<div id="giromobile-pay" class="giromobile-widget pay ' . $class . '" oid="' . $oid . '"  amount="' . $amount . '" currency="' . $currency . '">Pay with GiroConnect<div class="tid">' . $content . '</div></div>' . $this->_markupGeneral();
  }

  private function _markupGeneral() {
    static $sent;
    if (empty($sent)) {
      $sent = TRUE;
      $content = 'Please wait...';
      return '<div id="giromobileBackdrop"><div id="giromobileMsg">' . $content . '</div></div>';
    }
    return '';
  }
}
