<?php
/**
 * Main file for GiroConnect SDK.
 *
 * This is the main file of the GiroConnect SDK. Include this file
 * to load all required files to use the SDK.
 *
 * @package GiroConnect
 * @version SVN: $Revision: 117 $ ($Date: 2012-05-29 16:13:23 +0200 (Tue, 29 May 2012) $)
 */

/**
 * GiroConnect SDK Version.
 */
define('GIROCONNECT_VERSION', '1');

/**
 * Source ID to make an GiroConnect API call in testmode.
 */
define('GIROCONNECT_SOURCE_ID_TESTMODE', '6da0f976433e18641662198d98557955');

if (defined('GIROCONNECT_PSP_HOST_USER')) {
  /**
   * @ignore
   */
  define('GIROCONNECT_SERVER', 'http://' . GIROCONNECT_PSP_HOST_USER);
  /**
   * @ignore
   */
  define('GIROCONNECT_PSP_HOST', GIROCONNECT_PSP_HOST_USER);
}
else {
  /**
   * GiroConnect Server URL from GiroSolution AG.
   */
  define('GIROCONNECT_SERVER', 'https://www.girosolution.de');
  /**
   * Payment Service Provider URL for GiroSolution AG
   */
  define('GIROCONNECT_PSP_HOST', 'www.girosolution.de');
}

/**
 * Identifier for payment method giropay.
 */
define('GIROCONNECT_METHOD_GIROPAY',    'gc_giropay');
/**
 * Identifier for payment method GiroMobile.
 */
define('GIROCONNECT_METHOD_GIROMOBILE', 'gc_giromobile');
/**
 * Identifier for payment method GiroTreuhand.
 */
define('GIROCONNECT_METHOD_GIROTRUST',  'gc_girotrust');
/**
 * Identifier for payment method direct debit.
 */
define('GIROCONNECT_METHOD_DIRECTDEBIT','gc_directdebit');
/**
 * Identifier for payment method credit card.
 */
define('GIROCONNECT_METHOD_CREDITCARD', 'gc_creditcard');

/**
 * Identifier for path to GiroCockpit
 */
define('GIROCONNECT_PATH_COCKPIT', 0);
/**
 * Identifier for path to redirect url
 */
define('GIROCONNECT_PATH_REDIRECT', 1);
/**
 * Identifier for path to notify url
 */
define('GIROCONNECT_PATH_NOTIFY', 2);

/**
 * Continue status code
 */
define('GIROCONNECT_STATUS_CONTINUE', 1000);
/**
 * OK status code
 */
define('GIROCONNECT_STATUS_OK',       1001);
/**
 * Cancel status code
 */
define('GIROCONNECT_STATUS_CANCEL',   1002);

/**
 * Identifier for gender male
 */
define('GIROCONNECT_GENDER_MALE',    1);
/**
 * Identifier for gender female
 */
define('GIROCONNECT_GENDER_FEMALE',  2);
/**
 * Identifier for gender unknown
 */
define('GIROCONNECT_GENDER_UNKNOWN', 0);

include_once 'GiroConnectTools.class.inc';
include_once 'GiroConnectGiropay.class.inc';
include_once 'GiroConnectCreditcard.class.inc';
include_once 'GiroConnectDirectdebit.class.inc';
include_once 'GiroConnectGiromobile.class.inc';
include_once 'GiroConnectGirotrust.class.inc';
include_once 'GiroConnectAccount.class.inc';

/**
 * Main class for GiroConnect SDK
 *
 * Use it to get a payment method dependent Instance of GiroConnect.
 *
 * @package GiroConnect
 */
class GiroConnect {

  /**
   * Get a GiroConnect instance for a given payment method
   *
   * @param string $mid
   *   Merchant-ID from GiroCockpit
   *
   * @param string $pid
   *   Project-ID from GiroCockpit
   *
   * @param string $secret
   *   Project passphrase from GiroCockpit
   *
   * @param string $sourceId
   *   Source ID
   *
   * @param string $pm
   *   Payment method, use the payment method constants GIROCONNECT_METHOD_*
   *
   * @return FALSE|GiroConnectCreditcard|GiroConnectDirectdebit|GiroConnectGiromobile|GiroConnectGiropay|GiroConnectGirotrust
   *   FALSE in case of an error or a payment method dependent GiroConnect instance
   */
  static public function create($mid, $pid, $secret, $sourceId = GIROCONNECT_SOURCE_ID_TESTMODE, $pm = GIROCONNECT_METHOD_GIROPAY) {
    switch ($pm) {
      case GIROCONNECT_METHOD_GIROPAY:
        return new GiroConnectGiropay($mid, $pid, $secret, $sourceId);
        break;
      case GIROCONNECT_METHOD_CREDITCARD:
        return new GiroConnectCreditcard($mid, $pid, $secret, $sourceId);
        break;
      case GIROCONNECT_METHOD_DIRECTDEBIT:
        return new GiroConnectDirectdebit($mid, $pid, $secret, $sourceId);
        break;
      case GIROCONNECT_METHOD_GIROMOBILE:
        return new GiroConnectGiromobile($mid, $pid, $secret, $sourceId);
        break;
      case GIROCONNECT_METHOD_GIROTRUST:
        $lib = new GiroConnectGirotrust($mid, $pid, $secret, $sourceId);
        $lib->setCreatorId($mid);
        return $lib;
        break;
    }
    return FALSE;
  }
}

/**
 * Base class used for all GiroConnect payment method classes.
 *
 * @package GiroConnect
 */
abstract class GiroConnectAbstract {
  /**
   * Merchant ID from GiroCockpit
   *
   * @var string
   */
  protected $mid;
  /**
   * Project ID from GiroCockpit
   *
   * @var string
   */
  protected $pid;
  /**
   * Project passphrase from GiroCockpit
   *
   * @var string
   */
  protected $secret;
  /**
   * Transaction amount in EUR cent
   *
   * @var integer
   */
  protected $amount;
  /**
   * Transaction currency (only EUR is supported at the moment)
   *
   * @var string
   */
  protected $currency;
  /**
   * Source ID to identify the merchant platform system
   *
   * @var string
   */
  protected $sourceId;
  /**
   * Redirect url to redirect the user back to the shop after the transaction
   * is finished.
   *
   * @var string
   * @see setRedirectUrl()
   */
  protected $redirectUrl;
  /**
   * Notify url to send notification about the transaction result to the shop system.
   *
   * @var string
   * @see setNotifyUrl()
   */
  protected $notifyUrl;
  /**
   * Base informations
   *
   * @var array
   * @see GiroConnectTools::getBaseInfo()
   */
  protected $baseInfo;
  /**
   * Payment method constant
   *
   * @var string
   * @see GIROCONNECT_METHOD_CREDITCARD
   * @see GIROCONNECT_METHOD_DIRECTDEBIT
   * @see GIROCONNECT_METHOD_GIROMOBILE
   * @see GIROCONNECT_METHOD_GIROPAY
   * @see GIROCONNECT_METHOD_GIROTRUST
   */
  protected $pm;
  protected $is_ajax;
  protected $ajax_path;
  /**
   * Parameters to start a new payment transaction
   *
   * @var array
   * @see getTransactionParams()
   */
  protected $paymentParams = array();

  protected $autoRedirect = TRUE;
  protected $language = 'de';

  /**
   * Constructor
   *
   * Set some initial data
   *
   * @param string $mid
   *   Merchant-ID from GiroCockpit
   * @param string $pid
   *   Project-ID from GiroCockpit
   * @param string $secret
   *   Project passphrase from GiroCockpit
   * @param string $sourceId
   *   Source Id
   */
  public function __construct($mid = '', $pid = '', $secret = '', $sourceId = GIROCONNECT_SOURCE_ID_TESTMODE) {
    $this->baseInfo = GiroConnectTools::getBaseInfo();
    $this->is_ajax = FALSE;
    $this->mid = $mid;
    $this->pid = $pid;
    $this->secret = $secret;
    $this->sourceId = $sourceId;

    $this->paymentParams = array(
      'sourceId' => $this->sourceId,
      'merchantId' => $this->mid,
      'projectId' => $this->pid,
    );
  }

  public function isAjax() {
    return $this->is_ajax;
  }

  public function getLibPath() {
    return $this->baseInfo['lib_path'];
  }

  public function setBaseURL($base_url) {
    $this->baseInfo['base_url'] = $base_url;
  }

  /**
   * Set auto redirect for startTransaction
   *
   * @see GiroConnectCreditcard::startTransaction()
   * @see GiroConnectDirectdebit::startTransaction()
   * @see GiroConnectGiropay::startTransaction()
   *
   * @param bool $autoRedirect
   *   TRUE to activate auto redirect, FALSE otherwise
   */
  public function setAutoRedirect($autoRedirect) {
    $this->autoRedirect = $autoRedirect;
  }

  public function setAjaxPath($ajax_path) {
    $this->ajax_path = $ajax_path;
  }

  public function paymentMethod() {
    return $this->pm;
  }

  public function paymentSuccess($code) {
    return ($code == '4000');
  }

  /**
   * @return bool in case of an error or param array
   */
  protected function getTransactionParams() {
    $params = array(
      'libraryVersion' => GIROCONNECT_VERSION,
      'paymentMethod' => $this->pm,
      'language' => 'de',
    );
    foreach ($this->paymentFields as $field => $value) {
      if ($value === TRUE) {
        if (!isset($this->paymentParams[$field])) {
          throw new Exception('Required parameter "' . $field . '" missing.');
        }
        $params[$field] = $this->paymentParams[$field];
        continue;
      }
      if (!isset($this->paymentParams[$field])) {
        if (!empty($value)) {
          $params[$field] = $value;
        }
      }
      else {
        $params[$field] = $this->paymentParams[$field];
      }
    }

    $params['hash'] = GiroConnectTools::hash($params, $this->secret);
    return $params;
  }

  protected function gotoTransactionUrl($path = 'payment/start') {
    $result = $this->getTransactionUrl($path);
    if (($result['status'] == GIROCONNECT_STATUS_OK) && $this->autoRedirect) {
      header('location: ' . $result['redirect']);
      exit;
    }
    return $result;
  }

  protected function getTransactionUrl($path = 'payment/start') {
    $url = $this->generateTransactionUrl($path);
    $result = GiroConnectTools::httpRequest($url, array());

    if (!is_array($result) || !isset($result['status'])) {
      $result = array(
        'status' => GIROCONNECT_STATUS_OK,
        'redirect' => $result,
      );
    }
    return $result;
  }

  protected function generateTransactionUrl($path) {
    $this->paymentParams['urlRedirect'] = $this->redirectUrl;
    $this->paymentParams['urlNotify'] = $this->notifyUrl . '&amount=' . $this->paymentParams['amount'];

    $params = $this->getTransactionParams();

    return GiroConnectTools::getUrl(GIROCONNECT_SERVER . '/' . $path, $params);
  }

  public function setRedirectUrl($url) {
    $this->redirectUrl = GiroConnectTools::getUrl($url, array('pm' => $this->pm));
  }

  public function setNotifyUrl($url) {
    $this->notifyUrl = GiroConnectTools::getUrl($url, array('pm' => $this->pm));
  }

  public function getTextTitle() {
    return GiroConnectTools::getText('title', $this->pm, array(), $this->language);
  }

  public function getTextName() {
    return GiroConnectTools::getText('name', $this->pm, array(), $this->language);
  }

  public function getTextDescription() {
    return GiroConnectTools::getText('description', $this->pm, array(), $this->language);
  }

  public function getLogoURL($pm, $style = 'default') {
    return GIROCONNECT_SERVER . '/giroconnect/images/' . $style . '/' . $pm . '.png';
  }

  public function getLogoMarkup($pm) {
    $url   = $this->getLogoURL($pm);
    $alt   = GiroConnectTools::getText('name', $pm);
    $title = GiroConnectTools::getText('description', $pm);
    return '<img alt="' . $alt . '" title="' . $title . '" src="' . $url . '" />';
  }

  /**
   * Check hash for incoming redirect and notify calls
   *
   * Use this function to check all (expect GiroTreuhand) incoming redirect and
   * notify calls from GiroSolution AG.
   *
   * @return array
   *   In case of an error array('verified' => FALSE)
   *   Otherwise an array with merchantId, projectId, transactionId and gpCode
   */
  public function checkHash() {
    if (empty($_GET['order_id']) ||
      empty($_GET['gpCode']) ||
      empty($_GET['gpHash'])) {
      return array('verified' => FALSE);
    }
    $data = array(
      'merchantId' => $this->mid,
      'projectId' => $this->pid,
      'transactionId' => $_GET['order_id'],
      'gpCode' => $_GET['gpCode'],
    );
    $hash_check = $_GET['gpHash'];
    $hash = GiroConnectTools::hash($data, $this->secret);
    if ($hash === $hash_check) {
      $data['verified'] = TRUE;
    }
    else {
      $data['verified'] = FALSE;
    }
    return $data;
  }

  /**
   * @param $bankcode
   * @param $bankaccount
   * @return array
   *   status: either OK or CANCEL
   *   code: 'sb###'
   *   msg: clear text message
   */
  public function validateBankaccount($bankcode, $bankaccount) {
    $postdata = array(
      'merchantId' => $this->mid,
      'projectId'  => $this->pid,
      'bankcode'   => $bankcode,
      'bankaccount'=> $bankaccount,
    );
    $postdata['hash'] = GiroConnectTools::hash($postdata, $this->secret);

    $result = GiroConnectTools::httpRequest(GIROCONNECT_SERVER . '/payment/validate/bankaccount', $postdata, TRUE);

    if (!is_array($result) || !isset($result['status'])) {
      $result = array(
        'status' => ($result == 'sb200') ? GIROCONNECT_STATUS_OK : GIROCONNECT_STATUS_CANCEL,
        'code' => $result,
        'msg' => GiroConnectTools::getText($result),
      );
    }
    return $result;
  }

  public function serviceConvertBankaccount2Iban($bankcode, $bankaccount) {
    $postdata = array(
      'merchantId' => $this->mid,
      'projectId'  => $this->pid,
      'bankcode'   => $bankcode,
      'bankaccount'=> $bankaccount,
    );
    $postdata['hash'] = GiroConnectTools::hash($postdata, $this->secret);

    $result = GiroConnectTools::httpRequest(GIROCONNECT_SERVER . '/payment/convert/iban', $postdata, TRUE);

    if (!is_array($result)) {
      $result = array(
        'status' => GIROCONNECT_STATUS_CANCEL,
        'code' => $result,
      );
    }
    else {
      $result = array(
        'status' => ($result['code'] == 'sb200') ? GIROCONNECT_STATUS_OK : GIROCONNECT_STATUS_CANCEL,
        'iban' => $result,
      );
    }
    return $result;
  }

  public function serviceStatusTransaction($oid) {
    $postdata = array(
      'merchantId' => $this->mid,
      'projectId'  => $this->pid,
      'transactionId' => $oid,
    );
    $postdata['hash'] = GiroConnectTools::hash($postdata, $this->secret);

    $result = GiroConnectTools::httpRequest(GIROCONNECT_SERVER . '/payment/status/transaction', $postdata, TRUE);

    if (!is_array($result) || !isset($result['status'])) {
      $result = array(
        'status' => GIROCONNECT_STATUS_OK,
        'code' => $result,
        'msg' => GiroConnectTools::getText('gpCode' . $result, $this->paymentMethod()),
      );
    }
    return $result;
  }

}
