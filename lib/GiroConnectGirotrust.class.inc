<?php
/**
 * This file contains the GiroConnect subclass for GiroTreuhand transactions.
 *
 * @package GiroConnect
 */

/**
 * Description for class GiroConnectGirotrust
 *
 * @package GiroConnect
 * @example test_giropay.php How to use this function
 */
class GiroConnectGirotrust extends GiroConnectAbstract {
  protected $pm = GIROCONNECT_METHOD_GIROTRUST;

  protected $paymentFields = array(
    'sourceId' => TRUE,
    'trusteeId' => TRUE,
    'projectId' => TRUE,
    'creatorId' => TRUE,
    'merchantId' => TRUE,
    'transactionId' => TRUE,
    'amount' => TRUE,
    'currency' => 'EUR',
    'feesplit' => '',
    'urlRedirect' => TRUE,
    'urlNotify' => TRUE,
    'giropay' => '',
  );

  private $feeSplits = array();

  public function __construct($mid = '', $pid = '', $secret = '', $sourceId = GIROCONNECT_SOURCE_ID_TESTMODE) {
    parent::__construct($mid, $pid, $secret, $sourceId);
    $this->feeSplits = array(
      '0' => GiroConnectTools::getText('feesplit:0', GIROCONNECT_METHOD_GIROTRUST),
      '50' => GiroConnectTools::getText('feesplit:50', GIROCONNECT_METHOD_GIROTRUST),
      '100' => GiroConnectTools::getText('feesplit:100', GIROCONNECT_METHOD_GIROTRUST),
    );
  }

  public function setTrusteeId($trusteeId) {
    $this->paymentParams['trusteeId'] = $trusteeId;
  }

  public function setCreatorId($creatorId) {
    $this->paymentParams['creatorId'] = $creatorId;
  }

  public function getFeeSplits() {
    return $this->feeSplits;
  }

  public function startTransaction($amount, $currency, $transactionId, $feesplit = '50') {
    if (!array_key_exists($feesplit, $this->feeSplits)) {
      return array(
        'status' => GIROCONNECT_STATUS_CANCEL,
        'error' => TRUE,
        'error_msg' => 'Incorrect parameters given to function GiroConnectGirotrust::startTransactions().',
      );
    }
    $this->paymentParams['amount'] = $amount;
    $this->paymentParams['currency'] = $currency;
    $this->paymentParams['transactionId'] = $transactionId;
    $this->paymentParams['feesplit'] = $feesplit;

    return $this->gotoTransactionUrl('girotrust');
  }

  protected function getTransactionUrl($path = 'girotrust') {
    $url = $this->generateTransactionUrl($path);

    $result = array(
      'status' => GIROCONNECT_STATUS_OK,
      'redirect' => $url,
    );

    return $result;
  }

  /**
   * Check hash for incoming redirect and notify calls
   *
   * Use this function to check incoming GiroTreuhand redirect and notify
   * calls from GiroSolution AG.
   *
   * @return array
   *   In case of an error array('verified' => FALSE)
   *   Otherwise an array with merchantId, projectId, transactionId,
   *   contractId and status
   */
  public function checkHash() {
    if (empty($_GET['gtStatus']) ||
      empty($_GET['gtTransactionId']) ||
      empty($_GET['gtContractId']) ||
      empty($_GET['gtHash'])) {
      return array('verified' => FALSE);
    }
    $data = array(
      'merchantId' => $this->mid,
      'projectId' => $this->pid,
      'transactionId' => $_GET['gtTransactionId'],
      'contractId' => $_GET['gtContractId'],
      'status' => $_GET['gtStatus'],
    );
    $hash_check = $_GET['gtHash'];
    $hash = GiroConnectTools::hash($data, $this->secret);
    if ($hash === $hash_check) {
      $data['verified'] = TRUE;
    }
    else {
      $data['verified'] = FALSE;
    }
    return $data;
  }

  /**
   * Check if the GiroTreuhand contract is created
   *
   * @param string $code status from checkHash result
   * @return bool TRUE if contract in created, otherwise FALSE
   * @see checkHash()
   */
  public function contractCreated($code) {
    return ($code == '1000');
  }

  /**
   * Check if the GiroTreuhand contract is accepted by all
   * parties.
   *
   * @param string $code status from checkHash result
   * @return bool TRUE if contract is accepted, otherwise FALSE
   * @see checkHash()
   */
  public function contractAccepted($code) {
    return ($code == '2000');
  }
}
