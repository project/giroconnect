<?php
/**
 * This file contains the GiroConnect subclass for direct debit transactions.
 *
 * @package GiroConnect
 */

/**
 * Description for class GiroConnectDirectdebit
 *
 * @package GiroConnect
 */
class GiroConnectDirectdebit extends GiroConnectAbstract {
  protected $pm = GIROCONNECT_METHOD_DIRECTDEBIT;
  protected $paymentFields = array(
    'sourceId' => TRUE,
    'merchantId' => TRUE,
    'projectId' => TRUE,
    'transactionId' => TRUE,
    'amount' => TRUE,
    'currency' => 'EUR',
    'vwz' => TRUE,
    'bankcode' => '',
    'bankaccount' => '',
    'bankaccountowner' => '',
    'info1Label' => '',
    'info1Text' => '',
    'info2Label' => '',
    'info2Text' => '',
    'info3Label' => '',
    'info3Text' => '',
    'info4Label' => '',
    'info4Text' => '',
    'info5Label' => '',
    'info5Text' => '',
    'urlRedirect' => TRUE,
    'urlNotify' => TRUE,
  );

  public function startTransaction($amount, $currency, $transactionId, $text, $bankcode, $bankaccount) {
    $this->paymentParams['amount'] = $amount;
    $this->paymentParams['currency'] = $currency;
    $this->paymentParams['transactionId'] = $transactionId;
    $this->paymentParams['vwz'] = trim(substr($text, 0, 27));
    $this->paymentParams['bankcode'] = $bankcode;
    $this->paymentParams['bankaccount'] = $bankaccount;

    return $this->gotoTransactionUrl();
  }

}
