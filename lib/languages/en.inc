<?php
/**
 * German language file for GiroConnect SDK
 *
 * @package Language
 * @filesource
 */

/**
 * @var array
 */
$strings = array(
  'default' => array(
    'unknown' => '[ERROR]',
    'merchant id' => 'Merchant-ID',
    'project id' => 'Project-ID',
    'secret' => 'Project-Password',
    'go to cockpit' => 'To signup with [name] and/or create a new project for payments go to the [url].',
    'giromobile show login' => 'Show GiroMobile Login',
  ),
  GIROCONNECT_METHOD_GIROPAY => array(

  ),
  GIROCONNECT_METHOD_CREDITCARD => array(

  ),
  GIROCONNECT_METHOD_DIRECTDEBIT => array(

  ),
  GIROCONNECT_METHOD_GIROMOBILE => array(

  ),
  GIROCONNECT_METHOD_GIROTRUST => array(

  ),
);
