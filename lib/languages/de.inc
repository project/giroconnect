<?php
/**
 * German language file for GiroConnect SDK
 *
 * @package Language
 * @filesource
 */

/**
 * @var array
 */
$strings = array(
  'default' => array(
    'unknown' => '[FEHLER]',
    'merchant id' => 'Verkäufer-ID',
    'project id' => 'Projekt-ID',
    'secret' => 'Projekt-Passwort',
    'trustee id' => 'Treuhand-ID',
    'creator id' => 'Ersteller-ID',
    'creditcard types' => 'Kreditkarten-Typen',
    'go to cockpit' => 'Ihre Projekt-Daten erhalten Sie in Ihrem GiroCockpit unter www.girosolution.de/user.',
    'giromobile show login' => 'GiroMobile in Login-Formularen anzeigen',
    'sb200' => 'Bankverbindung ist valide.',
    'sb291' => 'Bankverbindung ist nicht valide.',
    'sb292' => 'Bankleitzahl ist ungültig.',
    'sb293' => 'Kontonummer darf maximal 10 Zeichen lang sein.',
    'sb299' => 'Ungültige Parameter.',
    'sb999' => 'Ungültige Zugangsdaten.',
    'order number' => 'Bestell-Nummer',
    'bank code' => 'Bankleitzahl',
    'bank account number' => 'Kontonummer',
    'continue to payment' => 'Weiter zur Bezahlung',
    'back to previous' => 'Zurück',
    'gateway unavailable' => 'Payment-Gateway nicht verfügbar',
    'http error' => 'Fehler bei http-Aufruf.',
    'title: payment completed' => 'Bezahlung abgeschlossen',
    'purpose' => 'Verwendungszweck',
    'amount' => 'Betrag',

  ),
  GIROCONNECT_METHOD_GIROPAY => array(
    'title' => 'giropay',
    'name' => 'giropay',
    'description' => 'Mit giropay zahlen Sie im Internet einfach, schnell und sicher per Online-Überweisung. Das Online-Zahlverfahren giropay wurde innerhalb der deutschen Kreditwirtschaft entwickelt und hat mit der Postbank, den Sparkassen und den Volksbanken Raiffeisenbanken starke vertrauensvolle Partner, die höchste Sicherheitsstandards garantieren.',
    'gpCode1900' => 'Fehler bei der Bezahlung über giropay.',
    'gpCode1910' => 'Fehler bei der Bezahlung über giropay.',
    'gpCode3100' => 'Sie haben die Bezahlung über giropay abgebrochen.',
    'gpCode4000' => 'Vielen Dank für die Bezahlung über giropay.',
    'gpCode4500' => 'Es ist ein unbekannter Fehler bei der Bezahlung über giropay aufgetreten. Bitte überprüfen Sie Ihre Kontoumsätze und wenden Sie sich bei Fragen an uns.',
    'gpCode4900' => 'Fehler bei der Bezahlung über giropay.',
    'sb100' => 'Die angegebene Bank unterstützt giropay und ist verfügbar.',
    'sb191' => 'Ihr Projekt befindet sich im Test-Mode, bitte nutzen Sie 12345679 als Test-BLZ.',
    'sb192' => 'Falsche Parameter bei Abfrage des Banken-Status.',
    'sb198' => 'Die angegebene Bank unterstützt derzeit noch kein giropay.',
    'sb199' => 'Die angegebene Bank ist vorübergehend nicht erreichbar.',
    'sb900' => 'Ungültige Bankleitzahl.',
    'sb999' => 'Ungültige Zugangsdaten.',
  ),
  GIROCONNECT_METHOD_CREDITCARD => array(
    'title' => 'Kreditkarte',
    'name' => 'Kreditkarte',
    'description' => '',
    'gpCode4000' => 'Vielen Dank für die Bezahlung per Kreditkarte.',
    'gpCode4900' => 'Fehler bei der Bezahlung per Kreditkarte.',
    'ccType:FieldLabel' => 'Kreditkarten-Typ',
    'ccType:FieldDescription' => 'Bitte wählen Sie hier den Typ der Kreditkarte aus, mit der Sie bezahlen wollen. Zur Eingabe Ihrer Kreditkarten-Nummer leiten wir Sie später auf ein speziell gesichertes Formular weiter.',
    'accepted card types' => 'Akzeptierte Kreditkarten-Typen',
    'accept low security' => 'Akzeptiere Zahlungen ohne 3D-Secure',
    'accept low security:description' => 'Bitte beachten Sie die Ausfall-Risiken, wenn Sie diese Option aktivieren.',
    'pay mode' => 'Bezahlmodus',
    'pay mode:payonly' => 'Pay Only: Es werden nur Kreditkarten-Daten vom Kunden abgefragt',
    'pay mode:payplus' => 'Pay Plus: Zusätzlich wird die Rechnungsanschrift vom Kunden abgefragt',
    'pay mode:fullpay' => 'Full Pay: Zusätzlich wird die Lieferanschrift vom Kunden abgefragt',
    'payinfo:title' => 'Sie bezahlen mit Kreditkarte',
  ),
  GIROCONNECT_METHOD_DIRECTDEBIT => array(
    'title' => 'Lastschrift',
    'name' => 'Lastschrift',
    'description' => '',
    'gpCode4000' => 'Vielen Dank für die Bezahlung per Lastschrift.',
    'gpCode4900' => 'Fehler bei der Bezahlung per Lastschrift.',
  ),
  GIROCONNECT_METHOD_GIROMOBILE => array(
    'title' => 'GiroMobile',
    'name' => 'GiroMobile',
    'description' => '',
    'gpCodeXXX' => 'TODO',
    'payinfo:title' => 'So bezahlen Sie mit GiroMobile',
    'payinfo:description' => 'Starten Sie Ihre GiroMobile App auf Ihrem SmartPhone, wählen die gewünschte Bezahlmethode aus und fotografieren den QR-Code. Folgen Sie anschließend den Anweisungen am Bildschirm.',
    'logininfo:title' => 'So melden Sie sich mit GiroMobile an',
    'logininfo:description' => 'Starten Sie Ihre GiroMobile App auf Ihrem SmartPhone, wählen eine beliebige Bezahlmethode aus und fotografieren den QR-Code. Folgen Sie anschließend den Anweisungen am Bildschirm.',
  ),
  GIROCONNECT_METHOD_GIROTRUST => array(
    'title' => 'GiroTreuhand',
    'name' => 'GiroTreuhand',
    'description' => '',
    'gpCodeXXX' => 'TODO',
    'feesplit' => 'Gebührenaufteilung',
    'feesplit:0' => 'Treuhand-Gebühr ist komplett vom Käufer zu tragen.',
    'feesplit:50' => 'Treuhand-Gebühr wird zu gleichen Teilen getragen.',
    'feesplit:100' => 'Treuhandgebühr wird komplett vom Verkäufer übernommen.',
  ),
);
