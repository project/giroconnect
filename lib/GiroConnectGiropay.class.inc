<?php
/**
 * This file contains the GiroConnect subclass for giropay transactions.
 *
 * @package GiroConnect
 */

/**
 * Description for class GiroConnectGiropay
 *
 * @package GiroConnect
 * @example test_giropay.php How to use this function
 */
class GiroConnectGiropay extends GiroConnectAbstract {
  protected $pm = GIROCONNECT_METHOD_GIROPAY;
  protected $paymentFields = array(
    'sourceId' => TRUE,
    'merchantId' => TRUE,
    'projectId' => TRUE,
    'transactionId' => TRUE,
    'amount' => TRUE,
    'currency' => 'EUR',
    'vwz' => TRUE,
    'bankcode' => '',
    'bankaccount' => '',
    'bankaccountowner' => '',
    'info1Label' => '',
    'info1Text' => '',
    'info2Label' => '',
    'info2Text' => '',
    'info3Label' => '',
    'info3Text' => '',
    'info4Label' => '',
    'info4Text' => '',
    'info5Label' => '',
    'info5Text' => '',
    'urlRedirect' => TRUE,
    'urlNotify' => TRUE,
  );

  public function startTransaction($amount, $currency, $transactionId, $text, $bankcode) {
    $this->paymentParams['amount'] = $amount;
    $this->paymentParams['currency'] = $currency;
    $this->paymentParams['transactionId'] = $transactionId;
    $this->paymentParams['vwz'] = trim(substr($text, 0, 27));
    $this->paymentParams['bankcode'] = $bankcode;

    return $this->gotoTransactionUrl();
  }

  /**
   * Validate bankcode for giropay payment
   *
   * @param string $bankcode
   *   8 digit bankcode
   *
   * @return array|mixed
   */
  public function validateBankcode($bankcode) {
    $postdata = array(
      'merchantId' => $this->mid,
      'projectId'  => $this->pid,
      'bankcode'   => $bankcode,
    );
    $postdata['hash'] = GiroConnectTools::hash($postdata, $this->secret);

    $result = GiroConnectTools::httpRequest(GIROCONNECT_SERVER . '/payment/status/bank', $postdata, TRUE);

    if (!is_array($result) || !isset($result['status'])) {
      $result = array(
        'status' => ($result == 'sb100') ? GIROCONNECT_STATUS_OK : GIROCONNECT_STATUS_CANCEL,
        'code' => $result,
        'msg' => GiroConnectTools::getText($result, GIROCONNECT_METHOD_GIROPAY),
      );
    }
    return $result;
  }

}
