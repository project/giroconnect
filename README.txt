-- SUMMARY --

TODO

-- REQUIREMENTS --

* Drupal 7 and either Ubercart module 3.x or Drupal Commerce.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* For Ubercart: configure GiroConnect at Administration >> Store >> Configuration >> Payment methods
* For Commerce: TODO

-- CUSTOMIZATION --

None

-- TROUBLESHOOTING --

Not known yet

-- FAQ --

Yet to come

-- CONTACT --

Current maintainer:
* Jürgen Haas (jurgenhaas) - http://drupal.org/user/168924

This project has been sponsored by:
* PARAGON Executive Services GmbH
  Providing IT services as individual as the requirements. Find out more from http://www.paragon-es.de
* GiroSolution AG
  Payment Supply Chain. Find out more from https://www.girosolution.de
