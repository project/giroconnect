<?php

/**
 * @file
 * The Drupal Commerce file for the giroconnect module.
 */

function giroconnect_get_pm_settings($pm) {
  $parts = array($pm, 'commerce_payment_' . $pm);
  $instance_id = implode('|', $parts);
  $payment_method = commerce_payment_method_instance_load($instance_id);
  return $payment_method['settings'];
}

/**
 * Implements hook_commerce_payment_method_info().
 *
 * @return array
 *   TODO
 */
function giroconnect_commerce_payment_method_info() {
  $payment_methods = array();
  $i = 0;
  foreach (GiroConnectTools::paymentMethods() as $pm) {
    $mid = variable_get($pm . '_mid', '');
    $pid = variable_get($pm . '_pid', '');
    $secret = variable_get($pm . '_secret', '');
    $lib = GiroConnect::create($mid, $pid, $secret, GIROCONNECT_SOURCE_ID, $pm);
    $i++;
    $payment_methods[$pm] = array(
      'base' => 'giroconnect_commerce',
      'short_title' => $lib->getTextName(),
      'title' => $lib->getTextTitle(),
      'description' => $lib->getTextDescription(),
      'active' => TRUE,
      'terminal' => FALSE,
      'offsite' => TRUE,
      'offsite_autoredirect' => FALSE,
      'weight' => ($i - 11),
      'callbacks' => array(
        'settings_form' => 'giroconnect_commerce_' . $pm . '_settings_form',
      ),
    );
  }
  return $payment_methods;
}

function giroconnect_commerce_gc_giropay_settings_form($settings = NULL) {
  return giroconnect_pm_setting(GIROCONNECT_METHOD_GIROPAY, $settings);
}
function giroconnect_commerce_gc_girotrust_settings_form($settings = NULL) {
  return giroconnect_pm_setting(GIROCONNECT_METHOD_GIROTRUST, $settings);
}
function giroconnect_commerce_gc_giromobile_settings_form($settings = NULL) {
  return giroconnect_pm_setting(GIROCONNECT_METHOD_GIROMOBILE, $settings);
}
function giroconnect_commerce_gc_directdebit_settings_form($settings = NULL) {
  return giroconnect_pm_setting(GIROCONNECT_METHOD_DIRECTDEBIT, $settings);
}
function giroconnect_commerce_gc_creditcard_settings_form($settings = NULL) {
  return giroconnect_pm_setting(GIROCONNECT_METHOD_CREDITCARD, $settings);
}

/**
 * Payment method callback: submit form.
 */
function giroconnect_commerce_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  $pm = $payment_method['method_id'];
  $form = array();
  if (!empty($order->data[$pm])) {
    $pane_values += $order->data[$pm];
  }
  $form = array();
  giroconnect_cart_details($pm, $form, $pane_values, $payment_method['settings']);
  return $form;
}

/**
 * Payment method callback: submit form validation.
 */
function giroconnect_commerce_submit_form_validate($payment_method, $pane_form, $pane_values, $order, $form_parents = array()) {
  $pm = $payment_method['method_id'];
  // TODO: We may validate the given values per payment method.
  return TRUE;
}

/**
 * Payment method callback: submit form submission.
 */
function giroconnect_commerce_submit_form_submit($payment_method, $pane_form, $pane_values, $order, $charge) {
  $pm = $payment_method['method_id'];
  $order->data[$pm] = $pane_values;
}

function giroconnect_commerce_redirect_form($form, &$form_state, $order, $payment_method) {
  $pm = $payment_method['method_id'];
  $cancel_url = url('checkout/' . $order->order_id . '/payment/back/' . $order->data['payment_redirect_key'], array('absolute' => TRUE));
  $return_url = url('checkout/' . $order->order_id . '/payment/return/' . $order->data['payment_redirect_key'], array('absolute' => TRUE));

  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $currency = $wrapper->commerce_order_total->currency_code->value();
  $amount_cent = $wrapper->commerce_order_total->amount->value();
  $amount = round(commerce_currency_amount_to_decimal($amount_cent, $currency), 2);
  $purpose = 'Order No. ' . $order->order_id . '-' . $order->uid;
  $values = $order->data[$pm];

  giroconnect_payment_form($pm, $payment_method['settings'], &$form, $amount, $currency, $order->order_id, $purpose, $values, $cancel_url);
  return $form;
}

function giroconnect_commerce_checkout_page_info_alter(&$checkout_pages) {
  unset($checkout_pages['payment']['help']);
}

/**
 * TODO
 */
function giroconnect_commerce_completed() {
  if (empty($_GET['pm']) || empty($_GET['order_id'])) {
    return drupal_access_denied();
  }
  $order = commerce_order_load($_GET['order_id']);
  if (empty($order)) {
    return drupal_access_denied();
  }
  $payment_method = commerce_payment_method_instance_load($order->data['payment_method']);
  $lib = _giroconnect_init_library($payment_method['method_id'], $payment_method['settings']);
  $data = $lib->checkHash();
  if (empty($data['verified'])) {
    watchdog('giroconnect', 'Received redirect with invalid GET data: %var', array('%var' => print_r($data, TRUE)), WATCHDOG_ERROR);
    return drupal_access_denied();
  }
  watchdog('giroconnect', 'Received valid redirect with GET data: %var', array('%var' => print_r($data, TRUE)), WATCHDOG_INFO);
  $url = giroconnect_completed($data['gpCode'], $order->order_id);
  drupal_goto($url);
}

function giroconnect_completed($code, $order_id) {
  $order = commerce_order_load($order_id);
  $payment_method = commerce_payment_method_instance_load($order->data['payment_method']);
  $lib = _giroconnect_init_library($payment_method['method_id'], $payment_method['settings']);
  if ($lib->paymentSuccess($code)) {
    _giroconnect_commerce_payment_transaction($payment_method, $order);
    $url = url('checkout/' . $order->order_id . '/payment/return/' . $order->data['payment_redirect_key'], array('absolute' => TRUE));
  }
  else {
    $url = url('checkout/' . $order->order_id . '/payment/back/' . $order->data['payment_redirect_key'], array('absolute' => TRUE));
  }
  return $url;
}

/**
 * TODO
 */
function giroconnect_commerce_notify() {
  $result = '400';
  if (empty($_GET['pm']) || empty($_GET['order_id'])) {
    // Wrong request.
  }
  else {
    $order = commerce_order_load($_GET['order_id']);
    if (empty($order)) {
      // Wrong request.
    }
    else {
      $payment_method = commerce_payment_method_instance_load($order->data['payment_method']);
      $lib = _giroconnect_init_library($payment_method['method_id'], $payment_method['settings']);
      $data = $lib->checkHash();
      if (empty($data['verified'])) {
        watchdog('giroconnect', 'Received notification with invalid GET data: %var', array('%var' => print_r($data, TRUE)), WATCHDOG_ERROR);
      }
      else {
        watchdog('giroconnect', 'Received valid notification with GET data: %var', array('%var' => print_r($data, TRUE)), WATCHDOG_INFO);
        if ($lib->paymentSuccess($data['gpCode'])) {
          _giroconnect_commerce_payment_transaction($payment_method, $order);
        }
        $result = '200';
      }
    }
  }
  header('http_code: ' . $result, TRUE, $result);
  print $result;
  exit;
}

function _giroconnect_commerce_payment_transaction($payment_method, $order) {
  if ($order->data['giroconnect_payment_recorded']) {
    // Payment has already been recorded.
    return;
  }
  $order->data['giroconnect_payment_recorded'] = TRUE;
  commerce_order_save($order);

  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $currency = $wrapper->commerce_order_total->currency_code->value();
  $amount_cent = $wrapper->commerce_order_total->amount->value();
  $amount = round(commerce_currency_amount_to_decimal($amount_cent, $currency), 2);

  $transaction = commerce_payment_transaction_new($payment_method['method_id'], $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->amount = $amount_cent;
  $transaction->currency_code = $currency;
  $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
  commerce_payment_transaction_save($transaction);
}
