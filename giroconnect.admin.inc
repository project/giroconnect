<?php

/**
 * @file
 * The settings file for the giroconnect module.
 */

/**
 * TODO
 *
 * @return array
 *   TODO
 */
function giroconnect_admin_settings() {
  $form = array();
  $form['gc_giromobile_mid'] = array(
    '#type' => 'textfield',
    '#title' => GiroConnectTools::getText('merchant id'),
    '#default_value' => variable_get('gc_giromobile_mid', ''),
  );
  $form['gc_giromobile_pid'] = array(
    '#type' => 'textfield',
    '#title' => GiroConnectTools::getText('project id'),
    '#default_value' => variable_get('gc_giromobile_pid', ''),
  );
  $form['gc_giromobile_secret'] = array(
    '#type' => 'textfield',
    '#title' => GiroConnectTools::getText('secret'),
    '#default_value' => variable_get('gc_giromobile_secret', ''),
  );
  $form['giroconnect_giromobile_show_login'] = array(
    '#type' => 'checkbox',
    '#title' => GiroConnectTools::getText('giromobile show login'),
    '#default_value' => variable_get('giroconnect_giromobile_show_login', FALSE),
  );
  return system_settings_form($form);
}
